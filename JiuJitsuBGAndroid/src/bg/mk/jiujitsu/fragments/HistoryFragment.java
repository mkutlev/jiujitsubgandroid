package bg.mk.jiujitsu.fragments;

import java.io.File;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage.QuotaUpdater;
import android.webkit.WebView;
import android.widget.Toast;
import bg.mk.jiujitsu.JiuJitsuConstants;
import bg.mk.jiujitsu.activities.InformationActivity;
import bg.mk.jiujitsu.utils.CacheUtil;
import bg.mk.jujitsu.R;

public class HistoryFragment extends Fragment {

	private WebView mWebView;

	public HistoryFragment() {
		// Empty constructor required for fragment subclasses
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_history, container,
				false);
		mWebView = (WebView) rootView.findViewById(R.id.historyWebView);
		setUpWebView();

		getActivity().setTitle(getString(R.string.history_fragment_title));
		return rootView;
	}

	private void setUpWebView() {
		WebSettings settings = mWebView.getSettings();
		// Enables Bulgarian symbols correct displaying
		settings.setDefaultTextEncodingName("utf-8");

		mWebView.setWebChromeClient(new WebChromeClient() {

			@Override
			public void onReachedMaxAppCacheSize(long requiredStorage,
					long quota, QuotaUpdater quotaUpdater) {
				quotaUpdater.updateQuota(requiredStorage * 2);
			}

		});

		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.getSettings().setAppCacheMaxSize(
				JiuJitsuConstants.APPLICATION_CACHE_MAX_SIZE);

		File externalStorageCacheDir = CacheUtil
				.getExternalStorageCacheDir(getActivity());
		String appCachePath = null;
		if (externalStorageCacheDir != null && externalStorageCacheDir.exists()) {
			appCachePath = externalStorageCacheDir.getAbsolutePath();
		} else {
			appCachePath = getActivity().getCacheDir().getAbsolutePath();
		}

		mWebView.getSettings().setAppCachePath(appCachePath);
		mWebView.getSettings().setAllowFileAccess(true);
		mWebView.getSettings().setAppCacheEnabled(true);
		mWebView.getSettings()
				.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		// Load data from an URL
		mWebView.loadUrl(JiuJitsuConstants.HISTORY_URL);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_refresh: {
			if (((InformationActivity) getActivity()).isConnected()) {
				mWebView.reload();
			} else {
				Toast.makeText(this.getActivity(),
						getString(R.string.action_refresh_no_conn),
						Toast.LENGTH_SHORT).show();
			}
			return true;
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
