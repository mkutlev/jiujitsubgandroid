package bg.mk.jiujitsu.activities;

import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import bg.mk.jiujitsu.JiuJitsuConstants;
import bg.mk.jiujitsu.data.JiuJitsuData;
import bg.mk.jiujitsu.resources.ImageLoader;
import bg.mk.jujitsu.R;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.api.services.jiujitsubg.model.Article;

public class ArticleActivity extends YouTubeFailureRecoveryActivity {
	
	private static final String TAG = ArticleActivity.class.getSimpleName();
	
	public static final String ARTICLE_INDEX_KEY = "article_index_key";
	
	private Article mArticle;
	
	private TextView mTitleView;
	private TextView mAuthorView;
	private ImageView mImageView;
	private TextView mTextView;
	public ImageLoader mImageLoader; 
	private YouTubePlayerView mVideoPlayer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_article);
		Bundle extras = getIntent().getExtras();
		if(extras != null) {
			int articleIndex = extras.getInt(ARTICLE_INDEX_KEY);
			try {
				mArticle = JiuJitsuData.getInstance(this).getArticles().get(articleIndex);
			} catch(IndexOutOfBoundsException ex) {
				Log.e(TAG, "Error finding the article!", ex);
				finish();
			}
		} else {
			Log.e(TAG, "Article index is not provided!");
			finish();
		}
		
		mTitleView = (TextView) findViewById(R.id.title);
		mTitleView.setText(mArticle.getTitle());
		mAuthorView = (TextView) findViewById(R.id.author);
		mAuthorView.setText("Автор: " + mArticle.getAuthor());
		List<String> urls = mArticle.getImageUrls();
		if(urls != null && !urls.isEmpty()) {
			mImageView = (ImageView) findViewById(R.id.image);
			mImageView.setVisibility(View.VISIBLE);
			mImageLoader = new ImageLoader(this);
			mImageLoader.DisplayImage(urls.get(0), mImageView);
		}
		
		mTextView = (TextView) findViewById(R.id.text);
		mTextView.setText(mArticle.getText());
		if(mArticle.getVideoIds() != null && !mArticle.getVideoIds().isEmpty()) {
			mVideoPlayer = (YouTubePlayerView) findViewById(R.id.youtube_view);
			mVideoPlayer.setVisibility(View.VISIBLE);
			mVideoPlayer.initialize(JiuJitsuConstants.DEVELOPER_KEY, this);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.article, menu);
		return true;
	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider,
			YouTubePlayer player, boolean wasRestored) {
		if (!wasRestored) {
			player.cueVideos(mArticle.getVideoIds());
		}
	}

	@Override
	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return (YouTubePlayerView) findViewById(R.id.youtube_view);
	}

}