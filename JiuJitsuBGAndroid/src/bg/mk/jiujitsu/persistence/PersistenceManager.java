package bg.mk.jiujitsu.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import bg.mk.jiujitsu.JiuJitsuConstants;
import bg.mk.jiujitsu.data.JiuJitsuData;
import bg.mk.jiujitsu.persistence.OnLoadListener.LoadError;
import bg.mk.jiujitsu.persistence.OnPersistListener.PersistError;
import bg.mk.jiujitsu.utils.CacheUtil;

public class PersistenceManager {

	private static final String TAG = JiuJitsuData.class.getSimpleName();
	private static final String PERSISTED_FILE_NAME = "jiujitsudata";
	private static final long DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24;
	
	private Context mContext;
	
	public PersistenceManager(Context context) {
		mContext = context;
	}
	
	public void persistData(OnPersistListener listener) {
		new PersistDataTask(listener).execute();
	}
	
	public void loadPersistedData(OnLoadListener listener) {
		if(isDataPersisted()) {
			new LoadPersistentDataTask(listener).execute();
		} else {
			listener.onLoadFailed(LoadError.NO_DATA_PERSISTED);
		}
	}
	
	public boolean isDataPersisted() {
		File persistedFile = new File(CacheUtil.getExternalStorageCacheDir(mContext),
				PERSISTED_FILE_NAME);
		return persistedFile.exists();
	}
	
	public long timePersisted() {
		File persistedFile = new File(CacheUtil.getExternalStorageCacheDir(mContext),
				PERSISTED_FILE_NAME);
		return persistedFile.lastModified();
	}

	public boolean isPersistedDataStale() {
		long timeNow = Calendar.getInstance().getTimeInMillis();
		return  timeNow - timePersisted() > DAY_IN_MILLISECONDS;
	}

	public boolean deletePersistedData() {
		File persistedFile = new File(
				CacheUtil.getExternalStorageCacheDir(mContext),
				PERSISTED_FILE_NAME);
		return persistedFile.delete();
	}
	
	private class PersistDataTask extends AsyncTask<Void, Void, Void> {

		private OnPersistListener mListener;
		private PersistError mError;
		
		public PersistDataTask(OnPersistListener listener) {
			mListener = listener;
			mError = null;
		}
		
		@Override
		protected Void doInBackground(Void... unused) {
			File persistedFile = new File(CacheUtil.getExternalStorageCacheDir(mContext),
					PERSISTED_FILE_NAME);
			FileOutputStream fos = null;
			ObjectOutputStream oos = null;
			boolean keep = true;

			try {
				fos = new FileOutputStream(persistedFile);
				oos = new ObjectOutputStream(fos);
				oos.writeObject(JiuJitsuData.getInstance(mContext));
			} catch (Exception ex) {
				Log.e(TAG, "Error persisting data!", ex);
				keep = false;
				//TODO
				mError = PersistError.UNKNOWN;
			} finally {
				try {
					if (oos != null) {
						oos.close();
					}
					if (fos != null) {
						fos.close();
					}
					if (!keep) {
						persistedFile.delete();
					}
				} catch (Exception ex) { }
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if(mError == null) {
				mListener.onPersistFinished();
			} else {
				mListener.onPersistFailed(mError);
			}
		}
	}

	private class LoadPersistentDataTask extends AsyncTask<Void, Void, JiuJitsuData> {

		private OnLoadListener mListener;
		private LoadError mError;
		
		
		public LoadPersistentDataTask(OnLoadListener listener) {
			mListener = listener;
			mError = null;
		}
		
		@Override
		protected JiuJitsuData doInBackground(Void... unused) {
			File persistedFile = new File(CacheUtil.getExternalStorageCacheDir(mContext),
					PERSISTED_FILE_NAME);
			if (!persistedFile.exists()) {
				return null;
			}
			JiuJitsuData data = null;
			FileInputStream fis = null;
			ObjectInputStream is = null;

			try {
				fis = new FileInputStream(persistedFile);
				is = new ObjectInputStream(fis);
				data = (JiuJitsuData) is.readObject();
			} catch (Exception ex) {
				Log.e(TAG, "Error loading data!", ex);
				//TODO
				mError = LoadError.UNKNOWN;
			} finally {
				try {
					if (fis != null)
						fis.close();
					if (is != null)
						is.close();
				} catch (Exception e) {
				}
			}
			return data;
		}

		@Override
		protected void onPostExecute(JiuJitsuData data) {
			if(mError == null) {
				mListener.onLoadFinished(data);
			} else {
				mListener.onLoadFailed(mError);
			}
		}
	}
	
}
