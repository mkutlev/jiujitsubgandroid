package bg.mk.jiujitsu.fragments;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import bg.mk.jiujitsu.activities.EventActivity;
import bg.mk.jiujitsu.data.JiuJitsuData;
import bg.mk.jiujitsu.data.JiuJitsuData.DataType;
import bg.mk.jiujitsu.data.OnDataChangeListener;
import bg.mk.jiujitsu.datamodel.Event;
import bg.mk.jiujitsu.datamodel.EventComparator;
import bg.mk.jujitsu.R;

import com.google.api.client.util.DateTime;

public class EventsFragment extends ListFragment implements
		OnDataChangeListener {

	private JiuJitsuData mData;
	private EventsAdapter mAdapter;
	private boolean mIsDataLoading;

	private TreeSet<Event> mEventsDataTreeSet;

	public EventsFragment() {
		// Empty constructor required for fragment subclasses
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mData = JiuJitsuData.getInstance(getActivity());
		mData.addDataChangeListener(this);
		mIsDataLoading = false;
		mEventsDataTreeSet = new TreeSet<Event>(new EventComparator());

		if (!mData.getCompetitions().isEmpty()) {
			mEventsDataTreeSet.addAll(mData.getCompetitions());
		}
		if (!mData.getSeminars().isEmpty()) {
			mEventsDataTreeSet.addAll(mData.getSeminars());
		}
		if (!mData.getDemonstrations().isEmpty()) {
			mEventsDataTreeSet.addAll(mData.getDemonstrations());
		}

		// TODO
		mAdapter = new EventsAdapter(this.getActivity(),
				R.layout.event_list_item, mEventsDataTreeSet);
		mAdapter.setNotifyOnChange(false);
		setListAdapter(mAdapter);

		getActivity().setTitle(getString(R.string.events_fragment_title));
		View mainLayout = super.onCreateView(inflater, container,
				savedInstanceState);
		mainLayout.setBackgroundResource(R.drawable.articles_backgr);
		return mainLayout;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (!mEventsDataTreeSet.isEmpty()) {
			setListShown(true);
		} else if (mIsDataLoading) {
			setListShown(false);
		}
		// TODO
		setEmptyText(getString(R.string.events_no_data_message));

		requestEventsData();
	}

	@Override
	public void onDestroyView() {
		mData.removeDataChangeListener(this);
		super.onDestroyView();
	}

	@Override
	public void onDataChanged(Object data, DataType dataType) {
		// Notifies the adapter to change the data
		if (dataType.equals(DataType.COMPETITIONS)) {
			mEventsDataTreeSet.addAll(mData.getCompetitions());
			reloadAdapterData();
			requestEventsData();
		} else if (dataType.equals(DataType.SEMINARS)) {
			mEventsDataTreeSet.addAll(mData.getSeminars());
			reloadAdapterData();
			requestEventsData();
		} else if (dataType.equals(DataType.DEMONSTRATIONS)) {
			mEventsDataTreeSet.addAll(mData.getDemonstrations());
			reloadAdapterData();
			requestEventsData();
		}
	}

	private void reloadAdapterData() {
		mAdapter.clear();
		mAdapter.addAll(mEventsDataTreeSet);
		mAdapter.notifyDataSetChanged();
		mAdapter.setNotifyOnChange(false);
		mIsDataLoading = false;
		setListShown(true);
	}

	/**
	 * Request data to be downloaded if necessary. Otherwise just
	 * {@link #mIsDataLoading} flag is set to <code>false</code>
	 */
	private void requestEventsData() {
		if (mData.isLoading()) {
			mIsDataLoading = true;
		} else if (mData.getCompetitions().isEmpty()) {
			mData.requestData(DataType.COMPETITIONS);
			mIsDataLoading = true;
		} else if (mData.getSeminars().isEmpty()) {
			mData.requestData(DataType.SEMINARS);
			mIsDataLoading = true;
		} else if (mData.getDemonstrations().isEmpty()) {
			mData.requestData(DataType.DEMONSTRATIONS);
			mIsDataLoading = true;
		} else {
			mIsDataLoading = false;
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		// TODO
		 Intent intent = new Intent(this.getActivity(),
		 EventActivity.class);
		 startActivity(intent);
	}

	private static class EventsAdapter extends ArrayAdapter<Event> {

		private static final String DATE_PATTERN = "dd.MM.yyyy HH:mm";

		private int resource;
		private LayoutInflater inflater;

		public EventsAdapter(Context ctx, int resourceId, List<Event> objects) {
			super(ctx, resourceId, objects);
			resource = resourceId;
			inflater = LayoutInflater.from(ctx);
		}

		public EventsAdapter(Context ctx, int resourceId, TreeSet<Event> objects) {
			super(ctx, resourceId);
			addAll(objects);
			resource = resourceId;
			inflater = LayoutInflater.from(ctx);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			// create a new view of my layout and inflate it in the row
			convertView = (RelativeLayout) inflater.inflate(resource, null);

			Event event = (Event) getItem(position);
			TextView titleView = (TextView) convertView
					.findViewById(R.id.title);
			titleView.setText(event.getTitle());

			DateTime dateTime = event.getDate();
			String date;
			if(dateTime != null) {
				SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
				date = "Дата: " + dateFormat
						.format(new Date(dateTime.getValue()));
			} else {
				date = "Датата не е известна!";
			}
			
			TextView dateView = (TextView) convertView.findViewById(R.id.date);
			dateView.setText(date);
			return convertView;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_refresh:
			Toast.makeText(this.getActivity(),
					getString(R.string.action_refresh_no_conn),
					Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
