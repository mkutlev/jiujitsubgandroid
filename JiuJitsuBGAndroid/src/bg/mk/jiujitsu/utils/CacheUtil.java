package bg.mk.jiujitsu.utils;

import java.io.File;

import android.content.Context;
import android.os.Environment;

public final class CacheUtil {

	/**
	 * Checks if external storage is accessible and has write permissions and
	 * creates (if missing) a cache dir for the application. The dir name will
	 * be the name of the app's package ("/Android/data/[package]/cache/")
	 * 
	 * @param context used for extracting the package name
	 * @return file to the cache dir or null if there is no access to the
	 *         external storage
	 */
	public static File getExternalStorageCacheDir(Context context) {
		boolean externalStorageAvailable = false;
		boolean externalStorageWriteable = false;

		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			externalStorageAvailable = externalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			externalStorageAvailable = true;
			externalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but
			// all we need to know
			// is we can neither read nor write
			externalStorageAvailable = externalStorageWriteable = false;
		}

		if (externalStorageAvailable && externalStorageWriteable) {
			String cache = String.format("/Android/data/%s/cache/",
					context.getPackageName());
			File cacheDir = new File(Environment.getExternalStorageDirectory(),
					cache);
			if (!cacheDir.exists()) {
				cacheDir.mkdirs();
			}

			return cacheDir;
		}

		return null;
	}

}
