package bg.mk.jiujitsu.persistence;

public interface OnPersistListener {

	public enum PersistError {
		UNKNOWN
	}

	public void onPersistFinished();

	public void onPersistFailed(PersistError error);
	
}
