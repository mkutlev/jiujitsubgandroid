package bg.mk.jiujitsu.data;

import bg.mk.jiujitsu.data.JiuJitsuData.DataType;

public interface OnDataChangeListener {
	
	/**
	 * Called when data is changed.
	 * 
	 * @param data
	 * @param dataType
	 */
	public void onDataChanged(Object data, DataType dataType);
	
}
