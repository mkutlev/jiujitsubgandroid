package bg.mk.jiujitsu.download;

import bg.mk.jiujitsu.data.JiuJitsuData.DataType;

public interface OnDownloadListener {

	public enum DownloadError {
		NETWORK_UNAVAILABLE, OPERATION_CANCELED, DATA_TYPE_UNKNOWN, UNKNOWN
	}

	public void onDownloadFinished(Object data, DataType dataType);

	public void OnDownloadFailed(DataType dataType, DownloadError error);

}
