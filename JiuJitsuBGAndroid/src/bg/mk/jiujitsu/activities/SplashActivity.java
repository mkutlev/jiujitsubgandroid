package bg.mk.jiujitsu.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import bg.mk.jiujitsu.JiuJitsuConstants;
import bg.mk.jujitsu.R;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
	}

	@Override
	protected void onResume() {
		super.onResume();
		CountDownTimer timer = new CountDownTimer(JiuJitsuConstants.SPLASH_TIME, JiuJitsuConstants.SPLASH_TIME) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				System.out.println("onTick()");
			}
			
			@Override
			public void onFinish() {
				if (!SplashActivity.this.isFinishing()) {
					startActivity(new Intent(SplashActivity.this,
							InformationActivity.class));
					SplashActivity.this.finish();
				}
			}
		};
		timer.start();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}
	
}
