package bg.mk.jiujitsu;

import bg.mk.jiujitsu.data.JiuJitsuData;
import android.app.Application;

public class JiuJitsuApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		//Start loading persisted data or downloading it
		JiuJitsuData.getInstance(this);
	}

}
