package bg.mk.jiujitsu;


public class JiuJitsuConstants {
	
	private JiuJitsuConstants() {}
	
	public static final long SPLASH_TIME = 3 * 1000;//5 seconds 
	
//	public static final String CACHE_DIR_PATH = Environment.getExternalStorageDirectory().getPath() + "/JiuJitsuBG"; 

	/**
	 * Please replace this with a valid API key which is enabled for the YouTube
	 * Data API v3 service. Go to the <a
	 * href="https://code.google.com/apis/console/">Google APIs Console</a> to
	 * register a new developer key.
	 */
	public static final String DEVELOPER_KEY = "AIzaSyD2ehLWSHR7IHwmAZkNEMybpuBNY0H6Mlg";
	
	public static final String HISTORY_URL = "file:///android_asset/history.html";
	
	public static final String RULES_URL = "file:///android_asset/rules.html";
	
	public static final long APPLICATION_CACHE_MAX_SIZE = 1024 * 1024 * 8; // 8 MB

}
