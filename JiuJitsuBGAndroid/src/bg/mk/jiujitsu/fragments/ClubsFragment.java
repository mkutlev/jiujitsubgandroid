package bg.mk.jiujitsu.fragments;

import java.util.List;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import bg.mk.jiujitsu.activities.ClubActivity;
import bg.mk.jiujitsu.data.JiuJitsuData;
import bg.mk.jiujitsu.data.JiuJitsuData.DataType;
import bg.mk.jiujitsu.data.OnDataChangeListener;
import bg.mk.jiujitsu.resources.ImageLoader;
import bg.mk.jujitsu.R;

import com.google.api.services.jiujitsubg.model.Club;

public class ClubsFragment extends ListFragment implements OnDataChangeListener {

	private JiuJitsuData mData;
	private ClubsAdapter mAdapter;

	private boolean mIsDataLoading = false;

	public ClubsFragment() {
		// Empty constructor required for fragment subclasses
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mData = JiuJitsuData.getInstance(getActivity());
		mData.addDataChangeListener(this);
		if (mData.getClubs().isEmpty() && !mData.isLoading()) {
			mData.requestData(DataType.CLUBS);
			mIsDataLoading = true;
		} else if (mData.isLoading()) {
			mIsDataLoading = true;
		}

		mAdapter = new ClubsAdapter(this.getActivity(),
				R.layout.club_list_item, mData.getClubs());
		mAdapter.setNotifyOnChange(false);
		this.setListAdapter(mAdapter);
		getActivity().setTitle(getString(R.string.clubs_fragment_title));
		View mainLayout = super.onCreateView(inflater, container,
				savedInstanceState);
		mainLayout.setBackgroundResource(R.drawable.articles_backgr);
		return mainLayout;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setListShown(!mIsDataLoading);
		setEmptyText(getString(R.string.clubs_no_data_message));
	}

	@Override
	public void onDestroyView() {
		mData.removeDataChangeListener(this);
		super.onDestroyView();
	}

	@Override
	public void onDataChanged(Object data, DataType dataType) {
		if (dataType.equals(DataType.CLUBS)) {
			// Notifies the adapter to change the data
			mAdapter.clear();
			mAdapter.addAll(mData.getClubs());
			mAdapter.notifyDataSetChanged();
			mIsDataLoading = false;
			setListShown(true);
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(this.getActivity(), ClubActivity.class);
		intent.putExtra(ClubActivity.CLUB_INDEX_KEY, position);
		startActivity(intent);
	}

	private static class ClubsAdapter extends ArrayAdapter<Club> {

		private int mResource;
		private LayoutInflater mInflater;
		public ImageLoader mImageLoader;

		public ClubsAdapter(Context ctx, int resourceId, List<Club> objects) {
			super(ctx, resourceId, objects);
			mResource = resourceId;
			mInflater = LayoutInflater.from(ctx);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			/* create a new view of my layout and inflate it in the row */
			convertView = (RelativeLayout) mInflater.inflate(mResource, null);

			Club club = (Club) getItem(position);
			TextView nameView = (TextView) convertView.findViewById(R.id.title);
			nameView.setText(club.getName());

			List<String> imageUrls = club.getImageUrls();
			ImageView imageView = (ImageView) convertView
					.findViewById(R.id.icon);
			if (imageUrls != null && !imageUrls.isEmpty()) {
				mImageLoader = new ImageLoader(this.getContext());
				mImageLoader.DisplayImage(imageUrls.get(0), imageView);
			} else {
				imageView.setImageResource(R.drawable.club_holder);
			}

			return convertView;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_refresh:
			Toast.makeText(this.getActivity(),
					getString(R.string.action_refresh_no_conn),
					Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
