package bg.mk.jiujitsu.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import bg.mk.jiujitsu.download.DataDownloader;
import bg.mk.jiujitsu.download.OnDownloadListener;
import bg.mk.jiujitsu.persistence.OnLoadListener;
import bg.mk.jiujitsu.persistence.OnPersistListener;
import bg.mk.jiujitsu.persistence.PersistenceManager;

import com.google.api.services.jiujitsubg.model.Article;
import com.google.api.services.jiujitsubg.model.Club;
import com.google.api.services.jiujitsubg.model.Coach;
import com.google.api.services.jiujitsubg.model.Competition;
import com.google.api.services.jiujitsubg.model.Demonstration;
import com.google.api.services.jiujitsubg.model.Judge;
import com.google.api.services.jiujitsubg.model.Seminar;

public class JiuJitsuData implements Serializable, OnDownloadListener,
		OnLoadListener, OnPersistListener {

	private static final long serialVersionUID = -7900413979664080821L;
	
	public enum DataType {
		ARTICLES(1), COMPETITIONS(2), SEMINARS(4), DEMONSTRATIONS(8), CLUBS(16), COACHES(32), JUDGES(64);

		private final int mFlag;

		private DataType(int flag) {
			mFlag = flag;
		}

		public int getFlag() {
			return mFlag;
		}
	}
	
	public enum DataState {
		READY, DOWNLOADING, LOADING, STALE, NO_DATA
	}
	
	private static final String TAG = JiuJitsuData.class.getSimpleName();
	
	private static JiuJitsuData mInstance;
	private transient DataState mDataState;
	
	private transient PersistenceManager mPersistenceManager;
	private transient boolean mIsLoading;
	private transient boolean mIsLoaded;
	private transient boolean mIsPersisting;
	
	private transient int mDownloadedData;
	private transient int mFailedDownloadData;
	
	private transient List<OnDataChangeListener> mDataChangeListeners;

	private List<Article> mArticles;
	private List<Competition> mCompetitions;
	private List<Seminar> mSeminars;
	private List<Demonstration> mDemonstrations;
	private List<Club> mClubs;
	private List<Coach> mCoaches;
	private List<Judge> mJudges;

	// Private constructor prevents instantiation from other classes
	private JiuJitsuData(Context context) {
		mPersistenceManager = new PersistenceManager(context);
		mIsLoading = false;
		mIsLoaded = false;
		mIsPersisting = false;
		mDownloadedData = 0;
		mFailedDownloadData = 0;
		
		mDataState = DataState.NO_DATA;

		if (mPersistenceManager.isDataPersisted()
				&& !mPersistenceManager.isPersistedDataStale()) {
			loadData();
		} else {
			// mInstance.startDataDownload();
		}
	}
	
	public static JiuJitsuData getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new JiuJitsuData(context);
		}
		
		return mInstance;
	}

	/**
	 * Request data to be downloaded. Implement {@link OnDataChangeListener} to
	 * be notified when task is done
	 * 
	 * @param type
	 */
	public void requestData(DataType type) {
		DataDownloader downloader = new DataDownloader();
		downloader.downloadData(type, this);
	}
	
	@Deprecated
	public void startDataDownload() {
		DataDownloader downloader = new DataDownloader();
		
		downloader.downloadData(DataType.ARTICLES, this);
		downloader.downloadData(DataType.COMPETITIONS, this);
		downloader.downloadData(DataType.SEMINARS, this);
		downloader.downloadData(DataType.DEMONSTRATIONS, this);
		downloader.downloadData(DataType.CLUBS, this);
		downloader.downloadData(DataType.COACHES, this);
		downloader.downloadData(DataType.JUDGES, this);
	}
	
	/**
	 * Persist this object
	 */
	public void persist() {
		mIsPersisting = true;
		mPersistenceManager.persistData(this);
	}
	
	/**
	 * Load persisted data into this object
	 */
	protected void loadData() {
		mDataState = DataState.LOADING;
		mIsLoading = true;
		mPersistenceManager.loadPersistedData(this);
	}
	
	public boolean addDataChangeListener(OnDataChangeListener listener) {
		if(mDataChangeListeners == null) {
			mDataChangeListeners = new ArrayList<OnDataChangeListener>();
		}
		return mDataChangeListeners.add(listener);
	}
	
	public boolean removeDataChangeListener(OnDataChangeListener listener) {
		if(mDataChangeListeners != null) {
			return mDataChangeListeners.remove(listener);
		}
		return false;
	}
	
	private void dataChanged(Object data, DataType dataType) {
		if(mDataChangeListeners != null) {
			for (int i = 0; i < mDataChangeListeners.size(); i++) {
				mDataChangeListeners.get(i).onDataChanged(data, dataType);
			}
		}
	}

	/**
	 * Get state of the data
	 * 
	 * @return
	 */
	public DataState getDataState() {
		return mDataState;
	}
	
	@Override
	public void onDownloadFinished(Object data, DataType type) {
		setDownloadedFlag(type);
		setData(data, type);
		//TODO
		if(isDownloaded()) {
			persist();
		}
	}

	@Override
	public void OnDownloadFailed(DataType type, DownloadError errorType) {
		setFailedFlag(type);
		if (type == DataType.ARTICLES) {
			setArticles(getArticles());
		} else if (type == DataType.COMPETITIONS) {
			setCompetitions(getCompetitions());
		} else if (type == DataType.SEMINARS) {
			setSeminars(getSeminars());
		} else if (type == DataType.DEMONSTRATIONS) {
			setDemonstrations(getDemonstrations());
		} else if (type == DataType.CLUBS) {
			setClubs(getClubs());
		} else if (type == DataType.COACHES) {
			setCoaches(getCoaches());
		} else if (type == DataType.JUDGES) {
			setJudges(getJudges());
		}
		
		Log.e(TAG, "DownloadFailed: DataType is " + type);
	}
	
	private void setData(Object data, DataType type) {
		if (type == DataType.ARTICLES) {
			setArticles((List<Article>)data);
		} else if (type == DataType.COMPETITIONS) {
			setCompetitions((List<Competition>) data);
		} else if (type == DataType.SEMINARS) {
			setSeminars((List<Seminar>) data);
		} else if (type == DataType.DEMONSTRATIONS) {
			setDemonstrations((List<Demonstration>) data);
		} else if (type == DataType.CLUBS) {
			setClubs((List<Club>) data);
		} else if (type == DataType.COACHES) {
			setCoaches((List<Coach>) data);
		} else if (type == DataType.JUDGES) {
			setJudges((List<Judge>) data);
		}
	}
	@Override
	public void onPersistFinished() {
		mIsPersisting = false;
		mDownloadedData = 0;
		Log.e(TAG, "Persist Finished successful!!!");
	}

	@Override
	public void onPersistFailed(PersistError error) {
		mIsPersisting = false;
		Log.e(TAG, "Persist Failed!!! Error: " + error);
	}

	@Override
	public void onLoadFinished(JiuJitsuData data) {
		if(data != null) {
			setArticles(data.getArticles());
			setCompetitions(data.getCompetitions());
			setDemonstrations(data.getDemonstrations());
			setSeminars(data.getSeminars());
			setClubs(data.getClubs());
			setCoaches(data.getCoaches());
			setJudges(data.getJudges());
			mDataState = DataState.READY;
			mIsLoaded = true;
		} else {
			mDataState = DataState.NO_DATA;
		}
		mIsLoading = false;
		
	}

	@Override
	public void onLoadFailed(LoadError error) {
		if (error.equals(LoadError.NO_DATA_PERSISTED)
				|| error.equals(LoadError.UNKNOWN)) {
			mPersistenceManager.deletePersistedData();
			mDataState = DataState.NO_DATA;
			mIsLoaded = false;
			mIsLoading = false;
			
//			startDataDownload();
		}
	}

	public boolean isUpdated() {
		DataType[] types = DataType.values();
		for(int i=0;i<types.length;i++) {
			if(isDownloaded(types[i])) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns is data downloaded after {@link #startDataDownload()} is called.
	 * 
	 * @return true if all data is downloaded, false otherwise
	 */
	@Deprecated
	public boolean isDownloaded() {
		DataType[] types = DataType.values();
		for(int i=0;i<types.length;i++) {
			if(!isDownloaded(types[i])) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isDownloaded(DataType type) {
		return (mDownloadedData & type.getFlag()) == type.getFlag();
	}
	
	private void setDownloadedFlag(DataType type) {
		mDownloadedData = mDownloadedData | type.getFlag();
	}
	
	public boolean isDownloadFailed(DataType type) {
		return ((mFailedDownloadData & type.getFlag()) == type.getFlag());
	}
	
	private void setFailedFlag(DataType type) {
		mFailedDownloadData = mFailedDownloadData | type.getFlag();
	}
	
	public boolean isLoading() {
		return mIsLoading;
	}
	
	public boolean isLoaded() {
		return mIsLoaded;
	}
	
	public boolean isPersisting() {
		return mIsPersisting;
	}
	
	public boolean isPersisted() {
		return mPersistenceManager.isDataPersisted();
	}

	public List<Article> getArticles() {
		if(mArticles == null) {
			mArticles = new ArrayList<Article>();
		}
		return mArticles;
	}

	public void setArticles(List<Article> articles) {
		this.mArticles = articles;
		dataChanged(mArticles, DataType.ARTICLES);
	}

	public List<Competition> getCompetitions() {
		if(mCompetitions == null) {
			mCompetitions = new ArrayList<Competition>();
		}
		return mCompetitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		this.mCompetitions = competitions;
		dataChanged(mCompetitions, DataType.COMPETITIONS);
	}

	public List<Seminar> getSeminars() {
		if(mSeminars == null) {
			mSeminars = new ArrayList<Seminar>();
		}
		return mSeminars;
	}

	public void setSeminars(List<Seminar> seminars) {
		this.mSeminars = seminars;
		dataChanged(mSeminars, DataType.SEMINARS);
	}

	public List<Demonstration> getDemonstrations() {
		if(mDemonstrations == null) {
			mDemonstrations = new ArrayList<Demonstration>();
		}
		return mDemonstrations;
	}

	public void setDemonstrations(List<Demonstration> demonstrations) {
		this.mDemonstrations = demonstrations;
		dataChanged(mDemonstrations, DataType.DEMONSTRATIONS);
	}

	public List<Club> getClubs() {
		if(mClubs == null) {
			mClubs = new ArrayList<Club>();
		}
		return mClubs;
	}

	public void setClubs(List<Club> clubs) {
		this.mClubs = clubs;
		dataChanged(mClubs, DataType.CLUBS);
	}

	public List<Coach> getCoaches() {
		if(mCoaches == null) {
			mCoaches = new ArrayList<Coach>();
		}
		return mCoaches;
	}

	public void setCoaches(List<Coach> coaches) {
		this.mCoaches = coaches;
		dataChanged(mCoaches, DataType.COACHES);
	}

	public List<Judge> getJudges() {
		if(mJudges == null) {
			mJudges = new ArrayList<Judge>();
		}
		return mJudges;
	}

	public void setJudges(List<Judge> judges) {
		this.mJudges = judges;
		dataChanged(mJudges, DataType.JUDGES);
	}
	
}
