/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://code.google.com/p/google-apis-client-generator/
 * (build: 2013-06-05 16:09:48 UTC)
 * on 2013-06-11 at 14:18:17 UTC 
 * Modify at your own risk.
 */

package com.google.api.services.jiujitsubg.model;

import java.io.Serializable;

import bg.mk.jiujitsu.datamodel.Event;

/**
 * Model definition for Competition.
 * 
 * <p>
 * This is the Java data model class that specifies how to parse/serialize into
 * the JSON that is transmitted over HTTP when working with the . For a detailed
 * explanation see: <a
 * href="http://code.google.com/p/google-api-java-client/wiki/Json"
 * >http://code.google.com/p/google-api-java-client/wiki/Json</a>
 * </p>
 * 
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public final class Competition extends com.google.api.client.json.GenericJson
		implements Event {

	private static final long serialVersionUID = -3019941592229956767L;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private java.util.List<java.lang.String> competitorKeys;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private java.util.List<Competitor> competitors;

	static {
		// hack to force ProGuard to consider Competitor used, since otherwise
		// it would be stripped out
		// see
		// http://code.google.com/p/google-api-java-client/issues/detail?id=528
		com.google.api.client.util.Data.nullOf(Competitor.class);
	}

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private com.google.api.client.util.DateTime date;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private java.util.List<java.lang.String> imageUrls;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private java.lang.String info;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private java.util.List<java.lang.String> judgeKeys;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private Key key;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private java.lang.String keyAsString;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private Location location;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private java.lang.String title;

	/**
	 * The value may be {@code null}.
	 */
	@com.google.api.client.util.Key
	private java.util.List<java.lang.String> videoIds;

	/**
	 * @return value or {@code null} for none
	 */
	public java.util.List<java.lang.String> getCompetitorKeys() {
		return competitorKeys;
	}

	/**
	 * @param competitorKeys
	 *            competitorKeys or {@code null} for none
	 */
	public Competition setCompetitorKeys(
			java.util.List<java.lang.String> competitorKeys) {
		this.competitorKeys = competitorKeys;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public java.util.List<Competitor> getCompetitors() {
		return competitors;
	}

	/**
	 * @param competitors
	 *            competitors or {@code null} for none
	 */
	public Competition setCompetitors(java.util.List<Competitor> competitors) {
		this.competitors = competitors;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public com.google.api.client.util.DateTime getDate() {
		return date;
	}

	/**
	 * @param date
	 *            date or {@code null} for none
	 */
	public Competition setDate(com.google.api.client.util.DateTime date) {
		this.date = date;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public java.util.List<java.lang.String> getImageUrls() {
		return imageUrls;
	}

	/**
	 * @param imageUrls
	 *            imageUrls or {@code null} for none
	 */
	public Competition setImageUrls(java.util.List<java.lang.String> imageUrls) {
		this.imageUrls = imageUrls;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public java.lang.String getInfo() {
		return info;
	}

	/**
	 * @param info
	 *            info or {@code null} for none
	 */
	public Competition setInfo(java.lang.String info) {
		this.info = info;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public java.util.List<java.lang.String> getJudgeKeys() {
		return judgeKeys;
	}

	/**
	 * @param judgeKeys
	 *            judgeKeys or {@code null} for none
	 */
	public Competition setJudgeKeys(java.util.List<java.lang.String> judgeKeys) {
		this.judgeKeys = judgeKeys;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public Key getKey() {
		return key;
	}

	/**
	 * @param key
	 *            key or {@code null} for none
	 */
	public Competition setKey(Key key) {
		this.key = key;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public java.lang.String getKeyAsString() {
		return keyAsString;
	}

	/**
	 * @param keyAsString
	 *            keyAsString or {@code null} for none
	 */
	public Competition setKeyAsString(java.lang.String keyAsString) {
		this.keyAsString = keyAsString;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            location or {@code null} for none
	 */
	public Competition setLocation(Location location) {
		this.location = location;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public java.lang.String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            title or {@code null} for none
	 */
	public Competition setTitle(java.lang.String title) {
		this.title = title;
		return this;
	}

	/**
	 * @return value or {@code null} for none
	 */
	public java.util.List<java.lang.String> getVideoIds() {
		return videoIds;
	}

	/**
	 * @param videoIds
	 *            videoIds or {@code null} for none
	 */
	public Competition setVideoIds(java.util.List<java.lang.String> videoIds) {
		this.videoIds = videoIds;
		return this;
	}

	@Override
	public Competition set(String fieldName, Object value) {
		return (Competition) super.set(fieldName, value);
	}

	@Override
	public Competition clone() {
		return (Competition) super.clone();
	}

}
