package bg.mk.jiujitsu.activities;

import bg.mk.jujitsu.R;
import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * Demonstration of PreferenceFragment, showing a single fragment in an
 * activity.
 */
public class JiuJitsuPreferenceActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Display the fragment as the main content.
		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new PrefsFragment()).commit();
		getActionBar().setBackgroundDrawable(
				getResources().getDrawable(R.drawable.action_bar_backgr));
		getActionBar().setLogo(R.drawable.drawer_icon);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	public static class PrefsFragment extends PreferenceFragment {

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preferences);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View layout = super.onCreateView(inflater, container,
					savedInstanceState);
			layout.setBackgroundResource(R.drawable.articles_backgr);
			return layout;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
