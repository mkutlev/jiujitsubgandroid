package bg.mk.jiujitsu.download;

import java.io.IOException;
import java.util.List;

import android.os.AsyncTask;
import android.util.Log;
import bg.mk.jiujitsu.data.JiuJitsuData.DataType;
import bg.mk.jiujitsu.download.OnDownloadListener.DownloadError;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.jiujitsubg.Jiujitsubg;
import com.google.api.services.jiujitsubg.model.Article;
import com.google.api.services.jiujitsubg.model.ArticleCollection;
import com.google.api.services.jiujitsubg.model.Club;
import com.google.api.services.jiujitsubg.model.ClubCollection;
import com.google.api.services.jiujitsubg.model.Coach;
import com.google.api.services.jiujitsubg.model.CoachCollection;
import com.google.api.services.jiujitsubg.model.Competition;
import com.google.api.services.jiujitsubg.model.CompetitionCollection;
import com.google.api.services.jiujitsubg.model.Demonstration;
import com.google.api.services.jiujitsubg.model.DemonstrationCollection;
import com.google.api.services.jiujitsubg.model.Judge;
import com.google.api.services.jiujitsubg.model.JudgeCollection;
import com.google.api.services.jiujitsubg.model.Seminar;
import com.google.api.services.jiujitsubg.model.SeminarCollection;

public class DataDownloader {
	
	private static final String TAG = DataDownloader.class.getSimpleName();
	
	/**
	 * Service object that manages requests to the backend.
	 */
	private Jiujitsubg service;
	
	public DataDownloader() {
		Jiujitsubg.Builder builder = new Jiujitsubg.Builder(
				AndroidHttp.newCompatibleTransport(), new GsonFactory(), null);
		service = builder.build();
	}

	/**
	 * Queries for all competitions, spawning an AsyncTask to make the request.
	 */
	public void downloadData(DataType type, OnDownloadListener listener) {
		downloadData(type, listener, 0, null);
	}
	
	public void downloadData(DataType type, OnDownloadListener listener,
			int limit, List<String> keys) {
		new DownloadDataTask(type, listener, limit, keys).execute();
	}

	private class DownloadDataTask extends
			AsyncTask<Void, Void, List<?>> {

		private int mLimit = 0;
		private List<String> mKeys;
		private OnDownloadListener mListener;
		private final DataType mDataType;
		private DownloadError mError;

		public DownloadDataTask(DataType type,
				OnDownloadListener listener, int limit, List<String> keys) {
			super();
			mLimit = limit;
			mKeys = keys;
			mListener = listener;
			mDataType = type;
			mError = null;
		}

		@Override
		protected List<?> doInBackground(Void... unused) {
			try {
				switch (mDataType) {
				case ARTICLES:
					return requestArticles();
				case CLUBS:
					return requestClubs();
				case COACHES:
					return requestCoaches();
				case COMPETITIONS:
					return requestCompetitions();
				case DEMONSTRATIONS:
					return requestDemonstrations();
				case JUDGES:
					return requestJudges();
				case SEMINARS:
					return requestSeminars();
				default:
					mError = DownloadError.DATA_TYPE_UNKNOWN;
				}
			} catch (IOException ex) {
				Log.e(TAG, "Error quering " + mDataType, ex);
				mError = DownloadError.NETWORK_UNAVAILABLE;
				return null;
			} catch (Exception ex) {
				Log.e(TAG, "Error quering " + mDataType, ex);
				mError = DownloadError.UNKNOWN;
				return null;
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(List<?> data) {
			if (mError == null) {
				mListener.onDownloadFinished(data, mDataType);
			} else {
				mListener.OnDownloadFailed(mDataType, mError);
			}
		}
		
		private List<Article> requestArticles() throws IOException {
			Jiujitsubg.Articles.List request = service.articles().list();
			if (mLimit > 0) {
				request.set("limit", String.valueOf(mLimit));
			}
			if (mKeys != null) {
				request.set("keys", mKeys);
			}
			ArticleCollection articles = request.execute();
			if (articles != null) {
				return articles.getItems();
			}
			return null;
		}
		
		private List<Club> requestClubs() throws IOException {
			Jiujitsubg.Clubs.List request = service.clubs().list();
			if (mLimit > 0) {
				request.set("limit", String.valueOf(mLimit));
			}
			if (mKeys != null) {
				request.set("keys", mKeys);
			}
			ClubCollection clubs = request.execute();
			if (clubs != null) {
				return clubs.getItems();
			}
			return null;
		}
		
		private List<Coach> requestCoaches() throws IOException {
			Jiujitsubg.Coaches.List request = service.coaches().list();
			if (mLimit > 0) {
				request.set("limit", String.valueOf(mLimit));
			}
			if (mKeys != null) {
				request.set("keys", mKeys);
			}
			CoachCollection coaches = request.execute();
			if (coaches != null) {
				return coaches.getItems();
			}
			return null;
		}
		
		private List<Competition> requestCompetitions() throws IOException {
			Jiujitsubg.Competitions.List request = service.competitions()
					.list();
			if (mLimit > 0) {
				request.set("limit", String.valueOf(mLimit));
			}
			if (mKeys != null) {
				request.set("keys", mKeys);
			}
			CompetitionCollection competitions = request.execute();
			if (competitions != null) {
				return competitions.getItems();
			}
			return null;
		}
		
		private List<Demonstration> requestDemonstrations() throws IOException {
			Jiujitsubg.Demonstrations.List request = service.demonstrations().list();
			if (mLimit > 0) {
				request.set("limit", String.valueOf(mLimit));
			}
			if (mKeys != null) {
				request.set("keys", mKeys);
			}
			DemonstrationCollection demonstrations = request.execute();
			if (demonstrations != null) {
				return demonstrations.getItems();
			}
			return null;
		}
		
		private List<Judge> requestJudges() throws IOException {
			Jiujitsubg.Judges.List request = service.judges().list();
			if (mLimit > 0) {
				request.set("limit", String.valueOf(mLimit));
			}
			if (mKeys != null) {
				request.set("keys", mKeys);
			}
			JudgeCollection judges = request.execute();
			if (judges != null) {
				return judges.getItems();
			}
			return null;
		}
		
		private List<Seminar> requestSeminars() throws IOException {
			Jiujitsubg.Seminars.List request = service.seminars().list();
			if (mLimit > 0) {
				request.set("limit", String.valueOf(mLimit));
			}
			if (mKeys != null) {
				request.set("keys", mKeys);
			}
			SeminarCollection seminars = request.execute();
			if (seminars != null) {
				return seminars.getItems();
			}
			return null;
		}
		
	}

}
