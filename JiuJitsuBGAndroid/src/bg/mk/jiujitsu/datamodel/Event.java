package bg.mk.jiujitsu.datamodel;

import java.io.Serializable;

import com.google.api.services.jiujitsubg.model.Location;

public interface Event extends Serializable {

	public java.lang.String getKeyAsString();

	public java.lang.String getTitle();

	public com.google.api.client.util.DateTime getDate();

	public Location getLocation();

	public java.lang.String getInfo();

	public java.util.List<java.lang.String> getImageUrls();

	public java.util.List<java.lang.String> getVideoIds();

}
