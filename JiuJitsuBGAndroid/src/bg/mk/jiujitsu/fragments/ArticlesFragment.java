package bg.mk.jiujitsu.fragments;

import java.util.List;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import bg.mk.jiujitsu.activities.ArticleActivity;
import bg.mk.jiujitsu.data.JiuJitsuData;
import bg.mk.jiujitsu.data.JiuJitsuData.DataType;
import bg.mk.jiujitsu.data.OnDataChangeListener;
import bg.mk.jujitsu.R;

import com.google.api.services.jiujitsubg.model.Article;

public class ArticlesFragment extends ListFragment implements
		OnDataChangeListener {

	private JiuJitsuData mData;
	private ArticlesAdapter mAdapter;
	private boolean mIsDataLoading = false;

	public ArticlesFragment() {
		// Empty constructor required for fragment subclasses
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mData = JiuJitsuData.getInstance(getActivity());
		mData.addDataChangeListener(this);
		if (mData.getArticles().isEmpty() && !mData.isLoading()) {
			mData.requestData(DataType.ARTICLES);
			mIsDataLoading = true;
		} else if (mData.isLoading()) {
			mIsDataLoading = true;
		}

		mAdapter = new ArticlesAdapter(this.getActivity(),
				R.layout.article_list_item, mData.getArticles());
		mAdapter.setNotifyOnChange(false);
		this.setListAdapter(mAdapter);
		getActivity().setTitle(getString(R.string.articles_fragment_title));
		View mainLayout = super.onCreateView(inflater, container,
				savedInstanceState);
		mainLayout.setBackgroundResource(R.drawable.articles_backgr);
		return mainLayout;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setListShown(!mIsDataLoading);
		setEmptyText(getString(R.string.articles_no_data_message));
	}

	@Override
	public void onDestroyView() {
		mData.removeDataChangeListener(this);
		super.onDestroyView();
	}

	@Override
	public void onDataChanged(Object data, DataType dataType) {
		if (dataType.equals(DataType.ARTICLES)) {
			// Notifies the adapter to change the data
			mAdapter.clear();
			mAdapter.addAll(mData.getArticles());
			mAdapter.notifyDataSetChanged();
			mIsDataLoading = false;
			setListShown(true);
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(this.getActivity(), ArticleActivity.class);
		intent.putExtra(ArticleActivity.ARTICLE_INDEX_KEY, position);
		startActivity(intent);
	}

	private static class ArticlesAdapter extends ArrayAdapter<Article> {

		private int resource;
		private LayoutInflater inflater;

		public ArticlesAdapter(Context ctx, int resourceId,
				List<Article> objects) {
			super(ctx, resourceId, objects);
			resource = resourceId;
			inflater = LayoutInflater.from(ctx);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			// create a new view of my layout and inflate it in the row
			convertView = (RelativeLayout) inflater.inflate(resource, null);

			Article article = (Article) getItem(position);
			TextView titleView = (TextView) convertView
					.findViewById(R.id.title);
			titleView.setText(article.getTitle());

			TextView authorView = (TextView) convertView
					.findViewById(R.id.author);
			authorView.setText("Автор: " + article.getAuthor());

			return convertView;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_refresh:
			Toast.makeText(this.getActivity(),
					getString(R.string.action_refresh_no_conn),
					Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
