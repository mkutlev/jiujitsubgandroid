package bg.mk.jiujitsu.activities;

import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import bg.mk.jiujitsu.JiuJitsuConstants;
import bg.mk.jiujitsu.data.JiuJitsuData;
import bg.mk.jiujitsu.resources.ImageLoader;
import bg.mk.jujitsu.R;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.api.services.jiujitsubg.model.Club;

public class ClubActivity extends YouTubeFailureRecoveryActivity {

	private static final String TAG = ClubActivity.class.getSimpleName();

	public static final String CLUB_INDEX_KEY = "club_index_key";

	private Club mClub;

	private TextView mNameView;
	private ImageView mImageView;
	private TextView mInfoView;
	public ImageLoader mImageLoader;
	private YouTubePlayerView mVideoPlayer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_club);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			int clubIndex = extras.getInt(CLUB_INDEX_KEY);
			try {
				mClub = JiuJitsuData.getInstance(this).getClubs().get(clubIndex);
			} catch (IndexOutOfBoundsException ex) {
				Log.e(TAG, "Error finding the club!", ex);
				finish();
			}
		} else {
			Log.e(TAG, "Club index is not provided!");
			finish();
		}

		mNameView = (TextView) findViewById(R.id.club_name);
		mNameView.setText(mClub.getName());

		List<String> urls = mClub.getImageUrls();
		if (urls != null && !urls.isEmpty()) {
			mImageView = (ImageView) findViewById(R.id.club_image);
			mImageView.setVisibility(View.VISIBLE);
			mImageLoader = new ImageLoader(this);
			mImageLoader.DisplayImage(urls.get(0), mImageView);
		}

		mInfoView = (TextView) findViewById(R.id.club_info);
		mInfoView.setText(mClub.getInfo());
		if (mClub.getVideoIds() != null && !mClub.getVideoIds().isEmpty()) {
			mVideoPlayer = (YouTubePlayerView) findViewById(R.id.youtube_view);
			mVideoPlayer.setVisibility(View.VISIBLE);
			mVideoPlayer.initialize(JiuJitsuConstants.DEVELOPER_KEY, this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.club, menu);
		return true;
	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider,
			YouTubePlayer player, boolean wasRestored) {
		if (!wasRestored) {
			player.cueVideos(mClub.getVideoIds());
		}
	}

	@Override
	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return (YouTubePlayerView) findViewById(R.id.youtube_view);
	}

}
