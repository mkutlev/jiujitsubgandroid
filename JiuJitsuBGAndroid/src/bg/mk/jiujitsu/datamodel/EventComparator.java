package bg.mk.jiujitsu.datamodel;

import java.util.Calendar;
import java.util.Comparator;

public class EventComparator implements Comparator<Event> {
	
	// Empty constructor
	public EventComparator() {
		super();
	}

	@Override
	public int compare(Event first, Event second) {
		if (first.getKeyAsString().equals(second.getKeyAsString())) {
			// Equals - keys has must be unique
			return 0;
		}

		long timeNow = Calendar.getInstance().getTimeInMillis();
		long firstEventTime = first.getDate() != null ? first.getDate().getValue() : 0;
		long secondEventTime = second.getDate() != null ? second.getDate().getValue() : 0;
		if (firstEventTime > timeNow && secondEventTime > timeNow) {
			// The two events are upcoming
			// Sooner event will be first
			return firstEventTime < secondEventTime ? -1 : 1;
		} else if (firstEventTime < timeNow && secondEventTime < timeNow) {
			// The two events are past
			// Sooner event will be first
			return firstEventTime < secondEventTime ? 1 : -1;
		} else {
			// One of the events is upcoming, one is past. The upcoming event
			// will be first
			return firstEventTime < secondEventTime ? 1 : -1;
		}
	}

}
