package bg.mk.jiujitsu.persistence;

import bg.mk.jiujitsu.data.JiuJitsuData;

public interface OnLoadListener {

	public enum LoadError {
		NO_DATA_PERSISTED, UNKNOWN
	}
	
	public void onLoadFinished(JiuJitsuData data);

	public void onLoadFailed(LoadError error);

}
